package com.epam.rd.sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class SorterTest {

    private String userInput;
    private String expectedOutput;

    public SorterTest(String userInput, String expectedOutput) {
        this.userInput = userInput;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        return Arrays.asList(new Object[][]{
                {"q", "[]"},
                {"3\nq\n", "[3]"},
                {"4\n6\n5\n3\n7\n2\n8\n1\n9\n10\n", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"},
                {"4\n6\n5\n3\n7\n2\n8\n1\n9\n10\n11\n", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"}
        });
    }

    @Test
    public void testCornerCases() {
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        PrintStream stdout = System.out;
        System.setOut(ps);
        Sorter.main(null);

        System.setIn(stdin);
        System.setOut(stdout);

        String actual = byteArrayOutputStream.toString();
        assertTrue(actual.contains(expectedOutput));
    }
}
