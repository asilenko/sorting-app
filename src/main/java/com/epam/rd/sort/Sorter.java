package com.epam.rd.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class Sorter {
    private final Scanner scanner;

    public Sorter(Scanner scanner) {
        this.scanner = scanner;
    }

    public static void main(String[] args) {
        System.out.println("Sorted numbers: " + new Sorter(new Scanner(System.in)).readNumbers());
    }

    public List<Integer> readNumbers() {
        List<Integer> numbers = new ArrayList<>();
        boolean quit = false;

        System.out.println("Provide numbers to sort. Enter 'q' in case you don't want to provide any more data.");

        while (numbers.size() < 10 && !quit) {
            String nextLine = scanner.nextLine();
            if (nextLine.equals("q")) {
                quit = true;
                break;
            }
            try {
                int number = Integer.parseInt(nextLine);
                numbers.add(number);
            } catch (NumberFormatException e) {
                System.err.println("Provide integer value");
            }
        }
        Collections.sort(numbers);
        return numbers;
    }
}
